#include <Arduino.h>
#include <SPI.h>
#include <Ethernet.h>
#include <Servo.h>
#include <Wire.h>
#include <BH1750.h>
#include <BME280I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define PIN_ETHERNET     10
#define PIN_SERVO_DATA    9
#define PIN_HEATER        6
#define PIN_FAN           5
#define PIN_DALLAS       A0
#define POS_OPEN         10
#define POS_CLOSED       40
#define LUX_MAX        1500
#define LUX_MIN        1000
#define HEATER_MAX_TEMP  50
#define LOW_TEMP         10
#define NORMAL_TEMP      20
#define HIGHT_TEMP       30

byte              mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x23};
EthernetServer    server(80);
Servo             myservo;
BH1750            lightMeter(0x23);
OneWire           oneWire(PIN_DALLAS);
DallasTemperature sensors(&oneWire);
DeviceAddress     insideThermometer;
BME280I2C         bme;
BME280::TempUnit  tempUnit(BME280::TempUnit_Celsius);
BME280::PresUnit  presUnit(BME280::PresUnit_hPa);

void setShutter(boolean state);

float tempHeater  =  0;
float tempHousing =  0;
float lux         =  0;
float pressure    =  0;
float humidity    =  0;
float setTemp     = 23;
bool  heater      = false;
bool  fan         = false;
bool  shutter     = false;
char  linebuf[20];

void setupIO(void){
    pinMode(PIN_HEATER,      OUTPUT);
    pinMode(PIN_FAN,         OUTPUT);
}

void setupEthernet(void){
    Ethernet.init(PIN_ETHERNET);
    Ethernet.begin(mac);
    server.begin();
}

void setupServo(void){
    setShutter(true);
}

void setupI2C(void){
  Wire.begin();
  lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
  bme.begin();
}

void setupOneWire(void){
  sensors.begin();
  sensors.getAddress(insideThermometer, 0);
  sensors.setResolution(insideThermometer, 12);
}

void setup(){
    setupIO();
    setupEthernet();
    setupServo();
    setupI2C();
    setupOneWire();
}

void handleRequests(void){
    EthernetClient client = server.available();

    if (client) {
        uint8_t charcount = 0;
        memset(linebuf, 0, sizeof(linebuf));

        while (client.connected()) {

            if (client.available()) {
                char c = client.read();

                if (charcount < sizeof(linebuf) - 1){
                    linebuf[charcount] = c;
                    charcount++;
                }

                if (c == '\n'){

                    char *p0  = strstr(linebuf, "setTemp=0");
                    char *p5  = strstr(linebuf, "setTemp=5");
                    char *p10 = strstr(linebuf, "setTemp=10");
                    char *p15 = strstr(linebuf, "setTemp=15");
                    char *p20 = strstr(linebuf, "setTemp=20");
                    char *p25 = strstr(linebuf, "setTemp=25");
                    char *p30 = strstr(linebuf, "setTemp=30");


                    if( p0){ setTemp= 0; }
                    if( p5){ setTemp= 5; }
                    if(p10){ setTemp=10; }
                    if(p15){ setTemp=15; }
                    if(p20){ setTemp=20; }
                    if(p25){ setTemp=25; }
                    if(p30){ setTemp=30; }

                    client.println("HTTP/1.1 200 OK");
                    client.println("Content-Type: text/plain");
                    client.println("Connection: close");
                    // client.println("Refresh: 5");
                    client.println();
                    client.print("tempHeater ");
                    client.print(tempHeater);
                    client.print("\n");
                    client.print("tempHousing ");
                    client.print(tempHousing);
                    client.print("\n");
                    client.print("lux ");
                    client.print(lux);
                    client.print("\n");
                    client.print("pressure ");
                    client.print(pressure);
                    client.print("\n");
                    client.print("humidity ");
                    client.print(humidity);
                    client.print("\n");
                    client.print("setTemp ");
                    client.print(setTemp);
                    client.print("\n");
                    client.print("heater ");
                    client.print(heater);
                    client.print("\n");
                    client.print("fan ");
                    client.print(fan);
                    client.print("\n");
                    client.print("shutter ");
                    client.print(shutter);
                    client.print("\n");
                    break;
                }
            }
        }

        client.flush();
        client.stop();
    }
}

void measureStuff(void){
  lux        = lightMeter.readLightLevel();
  sensors.requestTemperatures();
  tempHeater = sensors.getTempC(insideThermometer);
  bme.read(pressure, tempHousing, humidity, tempUnit, presUnit);
}

void setShutter(boolean state){

    myservo.attach(PIN_SERVO_DATA);
    if (state){
        myservo.write(POS_CLOSED);
    }else{
        myservo.write(POS_OPEN);
    }

    delay(1000);
    myservo.detach();
    shutter = state;
}

void controllEnv(){

    if (heater){
        fan = true;
        if(tempHeater < HEATER_MAX_TEMP){
            digitalWrite(PIN_HEATER, HIGH);
        } else {
            digitalWrite(PIN_HEATER, LOW);
        }
    } else {
        digitalWrite(PIN_HEATER, LOW);
    }

    if (fan){
        digitalWrite(PIN_FAN, HIGH);
    } else {
        digitalWrite(PIN_FAN, LOW);
    }

}

void controllShutter(){

    if(shutter && lux < LUX_MIN){
        setShutter(false);
    }

    if(!shutter && lux > LUX_MAX){
        setShutter(true);
    }
}

void calculateEnv(void){

    if (tempHousing > HIGHT_TEMP){
        fan = true;
    } else {
        fan = false;
    }

    if (tempHousing < setTemp){
        heater = true;
    } else {
        heater = false;
    }
}

void loop(){
    measureStuff();
    calculateEnv();
    controllEnv();
    controllShutter();
    Ethernet.maintain();
    handleRequests();
}